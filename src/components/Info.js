import React from "react";

/**
 * This component will render info about each state/county
 * @module
 * @param {Object} props - Covid data
 * @returns {ReactElement} Info component
 * */
export default function Info(props) {
  return (
    <div data-testid="info-test" className="info">
      <span className="info--location">
        {!props.location ? "location" : props.location}
      </span>
      <table className="list">
        <tbody>
          <tr>
            <th className="category">Population:</th>
            <th>{props.population}</th>
          </tr>
          <tr>
            <th className="category">Cases:</th>
            <th>{props.cases}</th>
          </tr>
          <tr>
            <th className="category">Deaths: </th>
            <th>{props.deaths}</th>
          </tr>
        </tbody>
      </table>

      <table className="list">
        <thead>
          <tr>
            <th colSpan="2" className="innerheading">
              Testing
            </th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <th className="category">Positive Tests:</th>
            <th>{props.positiveTests}</th>
          </tr>
          <tr>
            <th className="category">Negative Tests:</th>
            <th>{props.negativeTests}</th>
          </tr>
          <tr>
            <th className="category">Test Positivity Ratio:</th>
            <th>{props.testRatio}</th>
          </tr>
          <tr>
            <th className="category">Contact Tracers:</th>
            <th>{props.contactTracers}</th>
          </tr>
        </tbody>
      </table>

      <table className="list">
        <thead>
          <tr>
            <th colSpan="2" className="innerheading">
              Vaccinations
            </th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <th className="category">First Dose:</th>
            <th>{props.firstDose}</th>
          </tr>
          <tr>
            <th className="category">Fully Vaccinated:</th>
            <th>{props.fullVacinated}</th>
          </tr>
          <tr>
            <th className="category">Additional Dose:</th>
            <th>{props.addDose}</th>
          </tr>
        </tbody>
      </table>

      <table className="list">
        <thead>
          <tr>
            <th colSpan="2" className="innerheading">
              Hospitalizations & Intensive Care Unit Hospitalizations
            </th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <th className="category">Hospitalizations:</th>
            <th>{props.hospitalizedCovid}</th>
          </tr>
          <tr>
            <th className="category">Intensive Care Unit Hospitalizations:</th>
            <th>{props.icuCovid}</th>
          </tr>
        </tbody>
      </table>
    </div>
  );
}
