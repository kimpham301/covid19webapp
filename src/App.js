import React, { Component } from "react";
import { Route, Routes } from "react-router-dom";
import Navbar from "./components/navbar/Navbar.js";
import Home from "./routes/Home.js";
import About from "./routes/aboutApp.js";
import Info from "./routes/aboutCovid.js";
import "./App.css";

class App extends Component {
  /**
   * @Class App
   * @returns {ReactElement} Routes for the application
   */
  render() {
    return (
      <React.Fragment>
        <Navbar />
        <Routes>
          <Route exact path="/" element={<Home />} />
          <Route exact path="aboutApp" element={<About />} />
          <Route exact path="aboutCovid" element={<Info />}></Route>
        </Routes>
      </React.Fragment>
    );
  }
}

export default App;
