import * as React from "react";
import "../App.css";

export default function aboutCovid() {
  return (
    <div data-testid="aboutCovid-test" className="aboutCovid">
      <main className="about--style">
        <h2>About the Covid-19 Disease</h2>
        <view>
          <p>
            Covid-19 is a viral respiratory disease caused by severe acute
            respiratory syndrome coronavirus 2 (SARS-CoV-2). This disease is
            transmitted through air, meaning that those who are infected can
            exhale those droplets and small airborne particles as they breathe,
            talk, cough, sneeze, or sing. The probabilty of transmission is
            higher when people are physically close, meaning that the disease
            spreads more quickly at crowded areas. Symptoms appear 2-14 days
            after exposure to the viral disease.
          </p>
          <p>
            The symptoms of this disease are developed by those who are
            infected:
          </p>
          <ul className="symptoms--list">
            <li>Fever or chills</li>
            <li>Cough</li>
            <li>Shortness of breath or difficulty breathing</li>
            <li>Fatigue</li>
            <li>Muscle or body aches</li>
            <li>Headache</li>
            <li>New loss of taste or smell</li>
            <li>Sore throat</li>
            <li>Congestion or runny nose</li>
            <li>Nausea or vomiting</li>
            <li>Diarrhea</li>
          </ul>
          <p>
            These are the emergency warning signs of this disease, which
            requires the infected person to seek medical care immediately:
          </p>
          <ul className="warningSigns--list">
            <li>Trouble breathing</li>
            <li>Persistent pain or pressure in the chest</li>
            <li>New confusion or unusual behavior</li>
            <li>Inability to wake or stay awake</li>
            <li>
              Pale, gray, or blue-colored skin, lips, or nail beds, depending on
              skin tone
            </li>
            <li>Severe abdominal pain</li>
          </ul>
          <p>Other symptoms of this disease are:</p>
          <ul className="otherSymptoms--list">
            <li>Pinkeye</li>
            <li>Swollen eyes</li>
            <li>Fainting</li>
            <li>Guillain-Barre Syndrome</li>
            <li>Coughing up blood</li>
            <li>Blood clots</li>
            <li>Seizures</li>
            <li>Heart problems</li>
            <li>Kidney damage</li>
            <li>Liver problems or damage</li>
          </ul>
          <p>The common symptoms in children and teenagers are:</p>
          <ul className="pediatricSymptoms--list">
            <li>Fever</li>
            <li>Cough</li>
            <li>Shortness of breath</li>
            <li>Many other symptoms that adults exhibit</li>
          </ul>
          <p>
            For children and teenagers, this can lead to Pediatric Multisystem
            Inflammatory Syndrome (PMIS).
          </p>
          <p>
            The Covid-19 disease is diagnosed through the use of reverse
            transcription polymerase chain reaction (RT-PCR) or other nucleic
            acid testing of infected secretions. Chest CT scans are also helpful
            in diagnosing this disease with high suspicion of infection.
            Detection of a past infection is done through serological tests,
            which detect antibodies produced by the body in response to the
            infection. This disease can also be diagnosed through a
            nasopharyngeal swab test, a rapid antigen test, a nasal swab test,
            or a sputum sample test.
          </p>
          <p>
            For mild cases, the symptoms of the disease are treated through
            supportive care such as medications like paracetamol or
            non-steroidal anti-inflammatory drugs (NSAIDs) to relieve symptoms
            (fever, body aches, cough), proper intake of fluids, rest, and nasal
            breathing. For more severe cases, treatment is done in the hospital.
            Dexameththasone is recommended for increasing levels of oxygen in
            people with low levels of oxygen, which reduces the risk of death.
            Noninvasive ventilation and admission to an intensive care unit for
            mechanical ventilation may be required to support breathing.
          </p>
          <p>
            To prevent infection from Covid-19, people must do the following:
          </p>
          <ul className="preventionMeasures--list">
            <li>Get vaccinated</li>
            <li>Get tested</li>
            <li>Stay home if possible</li>
            <li>Wear a mask in public</li>
            <li>Avoid crowded places if possible</li>
            <li>Keep distance from others</li>
            <li>Ventilation of indoor areas and spaces</li>
            <li>Management of potential exposure durations</li>
            <li>
              Wash hands with soap and water often and for at least 20 seconds
            </li>
            <li>Practice and maintain good respiratory hygiene</li>
            <li>
              Avoid touching the eyes, nose, and mouth with unwashed hands
            </li>
          </ul>
          <p>
            Those who are diagnosed with Covid-19 or who believe they may be
            infected must stay home except to get medical care, call ahead
            before visiting a healthcare provider, wear a face mask before
            entering the healthcare provider's office and when in any room or
            vehicle with another person, cover coughs and sneezes with a tissue,
            regularly wash hands with soap and water and avoid sharing personal
            household items.
          </p>
        </view>
      </main>
    </div>
  );
}
