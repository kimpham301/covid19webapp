import React, { useState } from "react";
import "../App.css";
import ReactTooltip from "react-tooltip";
import Map from "../components/Map";
import Carousel, { CarouselItem } from "../components/carousel";
import Info from "../components/Info.js";
import coviddata from "../data/coviddata";
import states from "../data/States.json";
import counties from "../data/Counties.json";
import Newsfeed from "../components/Newsfeed";
import References from "../components/References";

/**
 * @file Home.js is the home page of the app
 * @author Yrua Riley, Thanh Pham and David Nguyen
 * */
export default function Home() {
  const [content, setContent] = useState("");
  const [info, setInfo] = useState(coviddata);
  const covidInfo = (pl, ci) => {
    setInfo({
      location: ci.county == null ? pl.name : pl.name + " COUNTY, " + ci.state,
      population: ci.population.toLocaleString(),
      cases: ci.actuals.cases.toLocaleString(),
      deaths: ci.actuals.deaths.toLocaleString(),
      positiveTests:
        ci.actuals.positiveTests === null
          ? "N/A"
          : ci.actuals.positiveTests.toLocaleString(),
      negativeTests:
        ci.actuals.negativeTests === null
          ? "N/A"
          : ci.actuals.negativeTests.toLocaleString(),
      testRatio:
        ci.metrics.testPositivityRatio === null
          ? "N/A"
          : ci.metrics.testPositivityRatio,
      contactTracers:
        ci.actuals.contactTracers === null
          ? "N/A"
          : ci.actuals.contactTracers.toLocaleString(),
      firstDose:
        ci.actuals.vaccinationsInitiated === null
          ? "N/A"
          : ci.actuals.vaccinationsInitiated.toLocaleString(),
      fullVacinated:
        ci.actuals.vaccinationsCompleted === null
          ? "N/A"
          : ci.actuals.vaccinationsCompleted.toLocaleString(),
      addDose:
        ci.actuals.vaccinationsAdditionalDose === null
          ? "N/A"
          : ci.actuals.vaccinationsAdditionalDose.toLocaleString(),
      hospitalizedCovid:
        ci.actuals.hospitalBeds.currentUsageCovid === null
          ? "N/A"
          : ci.actuals.hospitalBeds.currentUsageCovid.toLocaleString(),
      icuCovid:
        ci.actuals.icuBeds.currentUsageCovid === null
          ? "N/A"
          : ci.actuals.icuBeds.currentUsageCovid.toLocaleString(),
    });
  };
  return (
    <div className="app">
      <div className="content">
        <div className="carousel">
          <Carousel>
            <CarouselItem>
              <Map
                setTooltipContent={setContent}
                handleClick={covidInfo}
                isState={true}
                mapUrl={states}
              />
            </CarouselItem>
            <CarouselItem>
              <Map
                setTooltipContent={setContent}
                handleClick={covidInfo}
                isState={false}
                mapUrl={counties}
              />
            </CarouselItem>
          </Carousel>
        </div>
        <ReactTooltip className="tooltip" multiline={true} html={true}>
          {content}
        </ReactTooltip>
        <div className="info">
          <Info {...info} />
        </div>
      </div>
      <div className="newsfeed">
        <Newsfeed />
      </div>
      <div className="references">
        <References />
      </div>
    </div>
  );
}
