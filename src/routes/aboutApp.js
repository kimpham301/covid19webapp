import * as React from "react";
import "../App.css";

export default function aboutApp() {
  return (
    <div data-testid="aboutApp-test" className="aboutApp">
      <main className="about--style">
        <h2>About the Covid-19 Informational Web Application</h2>
        <view>
          <p>
            The goal is to provide users, living in the United States, with
            access to current information regarding Covid-19 at a national,
            state, and county level, via a user-friendly web application.
            Covid-19 data will be collected from an existing API and rendered in
            the form of an interactive map of the United States. This app is
            developed using React and an existing Covid-19 API.
          </p>
          <p>
            The web application has the US map with the ability to hover over a
            state to display Covid-19 info for a state and the US county map
            with the ability to hover over a county to display Covid-19 info for
            a county. When hovering over a state or county, the web application
            will display the following info:
          </p>
          <ul className="parameters--list">
            <li>State/County</li>
            <li>Population</li>
            <li>Cases</li>
            <li>Deaths</li>
            <li>Positive Tests</li>
            <li>Negative Tests</li>
            <li>Test Positivity Ratio</li>
            <li>Contact Tracers</li>
            <li>First Dose</li>
            <li>Fully Vaccinated</li>
            <li>Additional Dose</li>
            <li>Hospitalizations</li>
            <li>ICU Hospitalizations</li>
          </ul>
        </view>
      </main>
    </div>
  );
}
